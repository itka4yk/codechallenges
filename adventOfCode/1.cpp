#include <iostream>
#include <string>

using namespace std;

string::size_type sz;

enum {
	NORTH,
	EAST,
	SOUTH,
	WEST
};

bool map[1000][1000] = {};

bool move(int orient, int &x, int &y, int steps)
{
	int xx, yy, finish;
	switch(orient)
	{
		case NORTH:
			finish = y + steps;
			do
			{
				y++;
				xx = x+500;
				yy = y+500;
				if(map[yy][xx])
					return true;
				map[yy][xx] = true;
			}while ( y < finish);
			break;

		case EAST:
			finish = x + steps;
			do
			{
				x++;
				xx = x+500;
				yy = y+500;
				if(map[yy][xx])
					return true;
				map[yy][xx] = true;
			}while ( x < finish);
			break;

		case SOUTH:
			finish = y - steps;
			do
			{
				y--;
				xx = x+500;
				yy = y+500;
				if(map[yy][xx])
					return true;
				map[yy][xx] = true;
			}while ( y > finish);
			break;

		case WEST:
			finish = x - steps;
			do
			{
				x--;
				xx = x+500;
				yy = y+500;
				if(map[yy][xx])
					return true;
				map[yy][xx] = true;
			}while ( x > finish);
			break;
	}
	return false;
}

int main()
{
	int x = 0, y = 0;
	int orientation = NORTH;
	string input = "";
	map[500][500] = true;
	do
	{
		input.clear();
		cin >> input;
		if(input[0] == 'R' || input[0] == 'L')
		{
			if(input[0] == 'R')
			{
				orientation = (orientation+1)%4;
			}
			else
				orientation = (orientation-1)%4;
			if(orientation < 0)
				orientation = 4 - abs(orientation);
			input.erase(0,1);
		}
		else
			break;
	}while(!move(orientation,x,y,stoi(input,&sz)));
	cout << abs(x)+ abs(y) << endl;
	return 0;
}