#include <iostream>
#include <utility>
using namespace std;




class numpad
{
private:
	const char pad[5][5] = {{'0','0','1','0','0'}, 
							{'0','2','3','4','0'},
							{'5','6','7','8','9'},
							{'0','A','B','C','0'},
							{'0','0','D','0','0'}};

	pair <int,int> currentPos;

public:
	numpad()
	{
		currentPos.first = 2; // y
		currentPos.second = 0; // x
	}

	char getNumber()
	{
		return pad[currentPos.first][currentPos.second];
	}
	void move(char c)
	{
		if(canMove(nextMove(c)))
			currentPos = nextMove(c);
	}

	pair<int,int> nextMove(char c)
	{
		pair<int,int> nextMove = currentPos;

		switch(c)
		{
			case 'U':
				nextMove.first--;
				break;
			case 'D':
				nextMove.first++;
				break;
			case 'L':
				nextMove.second--;
			break;
			case 'R':
				nextMove.second++;
			break;
		}
		return nextMove;
	}

	bool canMove(pair <int,int> nextMove)
	{
		return pad[nextMove.first][nextMove.second] != '0' && nextMove.first >= 0 && nextMove.first <= 4 && nextMove.second >= 0 && nextMove.second <= 4;
	}

};

int main()
{
	string output;
	string str = "";
	numpad bathroomPad;
	for(int i = 0; i < 5; i++)
	{	
		str.clear();
		cin >> str;
		for(int i = 0; i < str.length();i++)
			bathroomPad.move(str[i]);
		output += bathroomPad.getNumber();
	}
	cout << output << endl;
	


	return 0;
}