
#include <iostream>
#include <ctime>

using namespace std;

bool isPow2(long long int x)
{
	return ((x != 0) && ((x & (~x+1)) == x));
}

int main()
{
	long long int k;
	cin >> k;
	if(isPow2(k) && k >= 2)
		cout << "TAK " << endl;
	else 
		cout << "NIE" << endl;

	return 0;
}