#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int n, k;
int ilosc = 0;
vector<int> friends;
vector<int> colors;

bool compareColors(int color, int it)
{
	if (colors[it] >= color)
	{
		colors.erase(colors.begin()+it);
		return true;
	}
	return false;
}

bool check()
{
	int color1, color2;
	bool found1 = false;
	bool found2 = false;
	for (int i = n - 1, it; i >= 0; i--)
	{
		if (colors[colors.size()-1] >= friends[i]) // jeden color da sie
		{
			ilosc++;
			colors.pop_back();
		}
		else
		{
			color1 = friends[i] / 5 * 2;
			color2 = friends[i] / 5 * 3;
			if (colors[colors.size() - 1] < color2)
			{
				return false;
			}

			it = colors.size()-1;
			found1 = false;
			found2 = false;
			
			while(colors[it] > color1 && it > 0)
			{
				it /= 2;
			}
			while (!found2 && it < colors.size())
			{
				if (found1 && colors[it] >= color2)
				{
					colors.erase(colors.begin()+it);
					found2 = true;
					ilosc++;
				}
				else if (colors[it] >= color1 && !found1)
				{
					colors.erase(colors.begin()+it);
					found1 = true;
					ilosc++;
					it--;
				}
				it++;
			}
			if (!found2)
				return false;
		}
	}
	return true;
}

int main()
{
	ios_base::sync_with_stdio(false);
	cin >> n;
	cin >> k;
	for (int i = 0, temp; i < n; i++)
	{
		cin >> temp;
		friends.push_back(temp * 5);
	}
	for (int i = 0, temp; i < k; i++)
	{
		cin >> temp;
		colors.push_back(temp);
	}
	sort(friends.begin(), friends.end());
	sort(colors.begin(), colors.end());
	if (check())
		cout << ilosc << endl;
	else
		cout << "NIE" << endl;
	return 0;
}