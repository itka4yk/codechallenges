// MODEL JASIA-BOHRA REC

#include <iostream>
#include <set>
#include <unordered_set>
#include <queue>
#include <time.h>
using namespace std;


short numberOfDividers;
long long dividers[10];
set<long long> orbits;

void recur(long long orbit)
{
	if(orbit > 0 && orbits.find(orbit) == orbits.end() )
	{
		orbits.insert(orbit);
		for(int i = 0; i < numberOfDividers; i++)
			recur(orbit/dividers[i]);
	}
}

int byHash(long long startOrbit)
{
	int size = 0;
	long long pom;
	queue <long long> myqueue;
	myqueue.push(startOrbit);
	size++;
	unordered_set<long long> myset;
	myset.reserve(10000000);
	myset.clear();
	do
	{
		for(int i = 0; i < numberOfDividers; i++)
		{
			pom = (myqueue.front()) / (dividers[i]);
			if( myset.count(pom)==0)
			{
				size++;
				myqueue.push(pom);
				myset.insert(pom);
			}
		}
		myqueue.pop();
	}while(!myqueue.empty());
	return size;
}

int unrecWithErase(long long startOrbit)
{
	orbits.insert(startOrbit);
	int count = 0;
	auto it = orbits.rbegin();
	do
	{
		for(int i = 0; i < numberOfDividers; i++)
			orbits.insert(*it/dividers[i]);
		count++;
		orbits.erase(*it);
	}while(!orbits.empty());
	return count;
}

int unrec(long long startOrbit)
{
	orbits.insert(startOrbit);
	auto it = orbits.rbegin();
	do
	{
		for(int i = 0; i < numberOfDividers; i++)
		{
			orbits.insert(*it/dividers[i]);
		}
		it++;
	}while(it!=orbits.rend());
	return orbits.size();
}


int main() {
	ios_base::sync_with_stdio(false);
	long long startOrbit;
	cin >> startOrbit;
	cin >> numberOfDividers;
	for(int i = 0; i < numberOfDividers; i++)
		cin >> dividers[i];
	cout << "1. Unrecurrent way:" << endl;
	int time = clock();
	std::cout << unrec(startOrbit) << endl;
	cout << "Time is: " << ((float)clock() - time) / CLOCKS_PER_SEC << " s" << endl << endl;
	orbits.clear();

	cout << "2. Unrecurrent way (with set erasing)" << endl;
	time = clock();
	cout << unrecWithErase(startOrbit) << endl;
	cout << "Time is: " << ((float)clock() - time) / CLOCKS_PER_SEC << " s" << endl << endl;
	orbits.clear();

	cout << "3. Reccurent way:" << endl;
	time = clock();
	recur(startOrbit);
	orbits.insert(0);
	cout << orbits.size() << endl;
	cout << "Time is: " << ((float)clock() - time) / CLOCKS_PER_SEC << " s" << endl << endl;
	orbits.clear();

	cout << "4. Unreccurent with unordered set (by hash tables)" << endl;
	time = clock();
	cout << byHash(startOrbit) << endl;
	cout << "Time is: " << ((float)clock() - time) / CLOCKS_PER_SEC << " s" << endl << endl;
	return 0;
}
